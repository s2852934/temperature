package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public Quoter(){
    }

    public double getBookPrice(String isbn){
        double temp = Double.parseDouble(isbn);
        return temp*1.8 + 32;
    }
}
